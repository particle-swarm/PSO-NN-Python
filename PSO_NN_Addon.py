import PSO_NN_Python as PSO
import numpy as np


def predict(x, NN):
    xVals = NN
    # X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])


    # 2 nodes on input layer, 4 nodes first hidden, 6 nodes second hidden, 1 output
    weights0 = np.array(xVals[0:8])
    weights1 = np.array(xVals[8:32])
    weights2 = np.array(xVals[32:38])

    weights0 = weights0.reshape(2, 4)
    weights1 = weights1.reshape(4, 6)
    weights2 = weights2.reshape(6, 1)

    layerOne = 1.0 / (1.0 + np.exp(-(np.dot(X, weights0))))
    layerTwo = 1.0 / (1.0 + np.exp(-(np.dot(layerOne, weights1))))
    layerThree = 1.0 / (1.0 + np.exp(-(np.dot(layerTwo, weights2))))

    return layerThree


def main():
    X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    # expected results y = np.array([[0, 1, 1, 0]]).T
    myNN = PSO.main()
    y_pred = predict(X, myNN)
    print(y_pred)


if __name__ == "__main__":
    main()

